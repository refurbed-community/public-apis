# Public refurbed APIs

Here you can find the API definitions of the public refurbed APIs.

## Merchant API

Please see `refurbed_merchant_api/refb/merchant/v1/README.md` for details.

## Affiliate Partner API

Please see `refurbed_partner_affiliate_api/refb/partner/affiliate/v1/README.md` for details.

